#!/bin/bash


if [ -d "round-template" ]; then rm -rf "round-template"; fi
if [ ! -d "round-template" ]; then mkdir "round-template"; fi
cp -r pedagogy storyboard experiment README.md round-template/

if [ ! -d "vlabs-conversion-template" ]
then 
	git clone http://vlabs.iitb.ac.in/gitlab/jai/vlabs-conversion-template.git
fi

cp -r round-template vlabs-conversion-template/
cd vlabs-conversion-template/
if [ ! -d "images" ]; then mkdir "images"; fi
cd round-template/experiment/ || exit
cp -r images/* ../../images/
cd simulation/ || exit
cp -r images/* ../../../images/

# git config --get remote.origin.url

# basename $(git remote get-url origin) .git
